#yt_scraper

##внешние зависимоости:
1.youtube-search-python

2.pytube

## Использование:
###CLI


yt_scraper.py -h
usage: youtube downloader [-h] [-s SEARCH] [-p PATH] [-c COUNT] [-d DURATION]

Скачивание списка выдачи youtube согласно указанному поиску

optional arguments:
  -h, --help            show this help message and exit
  
  -s SEARCH, --search SEARCH
                        что ищем, если слов более 1, то через "+"
  
  -p PATH, --PATH PATH  целевая папка
  
  -c COUNT, --count COUNT кол-во собираемых видео, более 500 не имеет смысла
  
  -d DURATION, --duration DURATION ограничение сверху длительности искомых видео

(с) Famiksir 2021. Автор программы, как всегда, не несет никакой
ответственности ни за что.


###Пример:
yt_scraper.py -s interview
-p D:\path -d 300 -c 10

downloading 0/10   Jake Paul Tries Interviewing Logan Paul Before The Fight And
He Gets HEATED  dur = 81

...