import asyncio
from youtubesearchpython.__future__ import VideosSearch
from pytube import YouTube
import os
import sys, argparse


def dur2sec(timestring):
    timelist = timestring.split(":")
    while len(timelist) < 3:
        timelist.insert(0, 0)
    return sum(x * int(t) for x, t in zip([3600, 60, 1], timelist))


def get_url_list(query, duration, number):
    videos_search = VideosSearch(query)
    videos_result = asyncio.run(videos_search.next())
    urlset = set()
    while len(urlset) < number:
        for item in videos_result['result']:
            if item['duration'] and dur2sec(item['duration']) < duration:
                urlset.add(item['link'])
                #print(item['link'])
            if len(urlset) >= number:
                break
        try:
            videos_result = asyncio.run(videos_search.next())
        except:
            pass

        if len(videos_result['result']) == 0:
            print('search is over')
            break
    return urlset


def downloader(videos_urlist, already_exists, path):
    for num, item in enumerate(videos_urlist):
        tube = YouTube(item)
        try:
            print('downloading {}/{}'.format(num+1, len(videos_urlist)), ' ', tube.title, ' dur =', tube.length)
            video = tube.streams.get_highest_resolution()
            if tube.title+'.mp4' not in already_exists:
                video.download(output_path=path)
            else:
                print('This video already exists in given path')
        except:
            print('downloading error... Go next')
            pass


def createParser():
    parser = argparse.ArgumentParser(
        prog='youtube downloader',
        description='Скачивание списка выдачи youtube согласно указанному поиску',
        epilog='(с) Famiksir 2021. Автор программы, как всегда, не несет никакой'
               ' ответственности ни за что.'
    )
    parser.add_argument('-s', '--search', default='', help='search query, '
                                                                    'if more than one word then w1"+"w2...')
    parser.add_argument('-p', '--PATH', default=r'D:', help='destination')
    parser.add_argument('-c', '--count', type=int, default=500, help='required number of videos')
    parser.add_argument('-d', '--duration', type=int, default=300, help='border on top of video duration')

    return parser


if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args(sys.argv[1:])

    if not os.path.exists(namespace.PATH):
        print('Given path is not exist...')
        exit()

    existsvideo = [os.path.split(path)[-1] for path in os.listdir(namespace.PATH)]

    my_rez = get_url_list(namespace.search, number=namespace.count, duration=namespace.duration)
    downloader(my_rez, existsvideo, path=namespace.PATH)


